import React, { Component } from 'react';

/**
 * createStubComponent - provides a flexible component for testing
 *
 * @param name
 * @param options
 * @returns {FactoredStubComponent}
 */
export function createStubComponent(name = 'FactoredStubComponent', options = {renderChildren: false, methods: {}}) {
    class FactoredStubComponent extends Component {
        render() {
            const { props } = this;
            return options.renderChildren ? <div>{props.children}</div> : <div />;
        }
    }

    FactoredStubComponent.displayName = name;

    for (const method in options.methods) {
        if (options.methods.hasOwnProperty(method)) {
            FactoredStubComponent.prototype[method] = options.methods[method];
        }
    }

    return FactoredStubComponent;
};
