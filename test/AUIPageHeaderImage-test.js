import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUIPageHeaderImage from '../src/AUIPageHeaderImage';

describe('AUIPageHeaderImage', () => {
    it('should render the correct AUI markup', () => {
        expect(shallow(<AUIPageHeaderImage />).html()).to.equal(`<div class="aui-page-header-image"><span class="aui-avatar aui-avatar-large aui-avatar-project"><span class="aui-avatar-inner"></span></span></div>`);
    });
});
