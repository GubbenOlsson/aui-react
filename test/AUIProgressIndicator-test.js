import React from 'react';
import { shallow } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';

import ProgresIndicator from '../src/AUIProgressIndicator';

chai.use(chaiEnzyme());

describe('AUIProgressIndicator', () => {
    it('should render default mark up', () => {
        const wrapper = shallow(<ProgresIndicator />);
        expect(wrapper).to.have.tagName('div');
        expect(wrapper).to.have.className('aui-progress-indicator');
        expect(wrapper).to.have.prop('role', 'progressbar');
        expect(wrapper).to.have.prop('tabIndex', '0');
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="0" role="progressbar" aria-valuemin="0" aria-valuenow="0" aria-valuemax="1" tabindex="0"><span class="aui-progress-indicator-value" style="width:0%"></span></div>');
    });

    it('should render progress bar with value 0.44', () => {
        const wrapper = shallow(<ProgresIndicator value={0.44} />);
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="0.44" role="progressbar" aria-valuemin="0" aria-valuenow="0.44" aria-valuemax="1" tabindex="0"><span class="aui-progress-indicator-value" style="width:44%"></span></div>');
    });

    it('should render progress bar with value 0.44, max of 1.76', () => {
        const wrapper = shallow(<ProgresIndicator value={0.44} max={1.76} />);
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="0.25" role="progressbar" aria-valuemin="0" aria-valuenow="0.44" aria-valuemax="1.76" tabindex="0"><span class="aui-progress-indicator-value" style="width:25%"></span></div>');
    });

    it('should re-render and change value according to props', () => {
        const wrapper = shallow(<ProgresIndicator value={0.2} max={2} />);
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="0.1" role="progressbar" aria-valuemin="0" aria-valuenow="0.2" aria-valuemax="2" tabindex="0"><span class="aui-progress-indicator-value" style="width:10%"></span></div>');
        wrapper.setProps({value: 1.2});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="0.6" role="progressbar" aria-valuemin="0" aria-valuenow="1.2" aria-valuemax="2" tabindex="0"><span class="aui-progress-indicator-value" style="width:60%"></span></div>');
        wrapper.setProps({value: 15});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="1" role="progressbar" aria-valuemin="0" aria-valuenow="2" aria-valuemax="2" tabindex="0"><span class="aui-progress-indicator-value" style="width:100%"></span></div>');
        wrapper.setProps({max: 0});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="1" role="progressbar" aria-valuemin="0" aria-valuenow="1" aria-valuemax="1" tabindex="0"><span class="aui-progress-indicator-value" style="width:100%"></span></div>');
        wrapper.setProps({max: -10});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="1" role="progressbar" aria-valuemin="0" aria-valuenow="1" aria-valuemax="1" tabindex="0"><span class="aui-progress-indicator-value" style="width:100%"></span></div>');
        wrapper.setProps({max: 0.1});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" data-value="1" role="progressbar" aria-valuemin="0" aria-valuenow="0.1" aria-valuemax="0.1" tabindex="0"><span class="aui-progress-indicator-value" style="width:100%"></span></div>');
        wrapper.setProps({indeterminate: true});
        expect(wrapper.html()).to.equal('<div class="aui-progress-indicator" role="progressbar" aria-valuemin="0" aria-valuenow="0.1" aria-valuemax="0.1" tabindex="0"><span class="aui-progress-indicator-value"></span></div>');
    });
});
