import React from 'react';
import PropTypes from 'prop-types';
import AUIPagePanel from './AUIPagePanel';

const AUIPageContent = ({ className, pageHeader, sidebar, nav, aside, style, children }) => (
    <section id="content" role="main" className={className}>
        {pageHeader}
        {sidebar}
        <AUIPagePanel nav={nav} aside={aside} style={style}>{children}</AUIPagePanel>
    </section>
);

AUIPageContent.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
    ]),
    nav: PropTypes.node,
    aside: PropTypes.node,
    sidebar: PropTypes.node,
    pageHeader: PropTypes.node,
    style: PropTypes.object,
    className: PropTypes.string
};

export default AUIPageContent;
