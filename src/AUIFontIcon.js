import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AUIFontIcon = ({ size, type, ...props }) => {
    const classes = classnames('aui-icon', `aui-iconfont-${type}`, {
        'aui-icon-small': size === 'small'
    });

    return (
        <span className={classes} {...props} />
    );
};

AUIFontIcon.propTypes = {
    size: PropTypes.oneOf(['small']),
    type: PropTypes.string,
    title: PropTypes.string
};

export default AUIFontIcon;
