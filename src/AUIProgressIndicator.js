import React from 'react';
import PropTypes from 'prop-types';

const defaultMax = 1;

const AUIProgressIndicator = ({max, value, indeterminate}) => {
    const maxParsed = max > 0 ? max : defaultMax;
    const valueParsed = Math.max(0, Math.min(value, maxParsed));
    const valAsPercent = Number((valueParsed / maxParsed * 100).toFixed(2));
    const valAsFraction = indeterminate ? null : Number((valueParsed / maxParsed).toFixed(6));
    const barStyle = indeterminate ? {} : {width: `${valAsPercent}%`};

    return (
        <div className="aui-progress-indicator"
            data-value={valAsFraction}
            role="progressbar"
            aria-valuemin="0"
            aria-valuenow={valueParsed}
            aria-valuemax={maxParsed}
            tabIndex="0"
        >
            <span className="aui-progress-indicator-value" style={barStyle} />
        </div>
    );
};

AUIProgressIndicator.propTypes = {
    indeterminate: PropTypes.bool,
    max: PropTypes.number,
    value: PropTypes.number,
};

AUIProgressIndicator.defaultProps = {
    indeterminate: false,
    max: defaultMax,
    value: 0,
};

export default AUIProgressIndicator;
