import React from 'react';
import PropTypes from 'prop-types';

const AUIPageHeader = ({ headerImage, headerText, breadcrumb  }) => (
    <header className="aui-page-header">
        <div className="aui-page-header-inner">
            {headerImage}
            <div className="aui-page-header-main">
                <h1>{headerText}</h1>
                {breadcrumb}
            </div>
        </div>
    </header>
);

AUIPageHeader.propTypes = {
    headerText: PropTypes.string,
    headerImage: PropTypes.node,
    breadcrumb: PropTypes.node
};

export default AUIPageHeader;
