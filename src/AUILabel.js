import React from 'react';
import PropTypes from 'prop-types';

const AUILabel = ({ children }) => {
    return <a className="aui-label">{children}</a>;
};

AUILabel.propTypes = {
    children: PropTypes.string
};

export default AUILabel;
