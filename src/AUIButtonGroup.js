import React from 'react';
import PropTypes from 'prop-types';

const AUIButtonGroup = ({ children }) => (
    <p className="aui-buttons">{children}</p>
);

AUIButtonGroup.propTypes = {
    children: PropTypes.array
};

export default AUIButtonGroup;
